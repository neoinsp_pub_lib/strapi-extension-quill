## Installation
1. `admin` 디렉토리를 strapi root 디렉토리에 추가한다.  
Add the `admin` into the strapi root dir.
2. `content-manager` 디렉토리는 `./extension`에 추가한다.  
Add the `content-manager` into the `./extention` dir.
3. `npm install react-quill`
4. `npm run build`

## Toolbar (Quill Config. For note.)
### font-size
폰트 옵션 추가할 때 수정해야 할 부분    
Changes needed when adding font options

* QuillEditor
	* `Size.whitelist`
	* `QuillEditor.modules.toolbar.container`
* quill.custom.css
	* `.ql-picker-label[data-value=]`
	* `.ql-picker-item[data-value=]`

## Version compatibility
Tested in Strapi v3.0.0-beta.20.3
